# Hapi.js Project and Component Bootstrapper

A CLI bootstrapper of a [hapi.js](http://hapijs.com/) full-stack project. This is loosely based on the Ruby on Rails generation and scaffolding.

## Requirements
- Node.js >=v0.10.32

## Installation
    $ npm install -g hapi-strapper   # sudo may be required

## CLI Syntax

### Help
    $ hapi-strap --help

### Create a new project
    $ hapi-strap --new <project_name>

Notes:

- The project will be created in a new directory immediately under the current working directory where the ```hapi-strap``` application is run.
- A directory with the same name as the project specified must not already exist in the current working directory for the project to be successfully created.

### Running the new project
    $ cd <project_name>
    $ npm test
    $ npm start
    $ open http://localhost:3000   # if being executed on a machine with a browser

Notes:

- The default port number used by the bootstrapped application is 3000. To change the port, edit the **config/default.json** file or set the **PORT** environment variable using, for example, ```$ export PORT=9999```.
- By default, the application will only be accessible from the machine where it is running. This is due to the host value in the **config/default.json** file being **127.0.0.1**. If you want to expose the application to other machines, change the host to, for example, **0.0.0.0** or set the **IP** environment variable.

### Creating a new model
    $ hapi-strap -m <item_name>

Notes:

- This will generate a new file in the **app/models** directory.
- The model includes a **schema** object, which is used by the underlying validation logic to set the data structure of the model. This is a [joi](https://www.npmjs.com/package/joi) data structure.

### Creating a new controller
_**Still to come...**_

### Creating a model, views, and controller (scaffolding)
    $ hapi-strap -s <item_name>
    $ npm test

Notes:

- This will generate a new model (as above), a controller under **app/controllers**, views for the controller in a directory matching the item's name (under **app/views**), as well as modify the **config/routes.coffee** file to add the RESTful routes for the controller. There will also be a specs test file created for the controller under **test/server**.

### Documentation
- Within the application itself, API documentation and routes are always available via the [/docs](http:localhost/docs).
- The routing table is included at the bottom of the home page of the application for your reference during development. If the **NODE_ENV** environment variable is set to "production", this table is not shown.

## License

MIT