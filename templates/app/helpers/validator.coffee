#
# Created by Mike Linde <mlinde@lintechsol.com> on 22/05/15.
#
Promise = require 'bluebird'
validate = Promise.promisify require('joi').validate

class Validator
  @validate: (jsonData, schema) ->
    options =
      allowUnknown: true
      skipFunctions: true
    return validate jsonData, schema, options

module.exports = Validator