#
# Created by Mike Linde <mlinde@lintechsol.com> on 18/05/15.
#

_ = require 'underscore'
Promise = require 'bluebird'

getRouteTable = (server) ->
  return new Promise (resolve, reject) ->
    routeTable = []
  
    hapiTable = server.table()[0].table
    for routeInTable in hapiTable
      do (routeInTable) ->
        route =
          method: routeInTable.method
          path: routeInTable.path
        if routeInTable.settings.tags?
          route.tags = routeInTable.settings.tags
        if routeInTable.settings.id?
          route.id = routeInTable.settings.id
        routeTable.push route
  
    routeTable = _.sortBy routeTable, (route) ->
      route.path
  
    resolve routeTable

exports.getRouteTable = getRouteTable