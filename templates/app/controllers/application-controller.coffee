#
# Created by Mike Linde <mlinde@lintechsol.com> on 17/05/15.
#

ControllersHelper = require '../helpers/controllers-helper'

class ApplicationController
  @home = (req, reply) ->
    context = {
      title: ControllersHelper.getFullPageTitle()
    }
    if process.env.NODE_ENV isnt 'production'
      require('../helpers/routes-helper').getRouteTable req.server
      .then (routes) ->
        context.debugRoutes = routes
        reply.view 'application/home', context
    else
      reply.view 'application/home', context

  @statusApiHandler = (req, reply) ->
    context =
      status: 'running'
    reply context

  @help = (req, reply) ->
    context = {
      title: ControllersHelper.getFullPageTitle('Help')
    }
    reply.view 'application/help', context

  @about = (req, reply) ->
    context = {
      title: ControllersHelper.getFullPageTitle('About')
    }
    reply.view 'application/about', context

  @contact = (req, reply) ->
    context = {
      title: ControllersHelper.getFullPageTitle('Contact')
    }
    reply.view 'application/contact', context

module.exports = exports = ApplicationController