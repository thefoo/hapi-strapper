#
# Created by Mike Linde <mlinde@lintechsol.com> on 22/05/15.
#
Promise = require 'bluebird'
fs = Promise.promisifyAll require 'fs-extra'
_ = require 'underscore'

module.exports = ->
  String.prototype.capitalize = ->
    return "#{@charAt(0).toUpperCase()}#{@slice 1}"

  theModule =
    doesItemExist: (itemName, parentDir) ->
      return new Promise (resolve, reject) ->
        fs.readdirAsync parentDir
        .then (files) ->
          resolve _.contains files, itemName
        .catch (err) ->
          reject err

  theModule