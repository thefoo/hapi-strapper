#
# Created by Mike Linde <mlinde@lintechsol.com> on 25/05/15.
#
require('../../templates/helpers')()
Promise = require 'bluebird'
mkdirp = Promise.promisify require 'mkdirp'
path = require 'path'
fs = Promise.promisifyAll require 'fs'
pluralize = require 'pluralize'
Handlebars = require 'handlebars'
_ = require 'underscore'

class ControllerGenerator
  templateDir = path.normalize "#{__dirname}/../../templates"

  constructor: (@newItem, @projectDir) ->

  createController = (newItem, newItemCapitalized, newItemPlural, newItemPluralCaptialized, projectDir) ->
    return new Promise (resolve, reject) ->
      controllersDir = path.normalize "#{projectDir}/app/controllers"
      controllerPath = path.normalize "#{controllersDir}/#{newItemPlural}-controller.coffee"
      mkdirp controllersDir
      .then -> fs.readFileAsync path.normalize("#{templateDir}/controller.handlebars"), encoding: 'utf8'
      .then (data) ->
        template = Handlebars.compile data
        result = template
          item: newItem
          itemCapitalized: newItemCapitalized
          itemPlural: newItemPlural
          itemPluralCaptialized: newItemPluralCaptialized
        fs.writeFileAsync controllerPath, result
        .then ->
          console.log "Created: #{controllerPath}"
          resolve()
      .catch (err) ->
        reject err

  createRoute = (newItem, newItemPlural, projectDir) ->
    return new Promise (resolve, reject) ->
      serverPath = path.normalize "#{projectDir}/app/index"
      require(serverPath)()
      .then (server) ->
        require('../get-route-table') server
      .then (routes) ->
        found = _.findWhere routes, id: "#{newItemPlural}.index"
        if found?
          console.log 'Please note that the route for this type already exists. Not recreating it.'
          resolve()
        else
          routeFilePath = path.normalize "#{projectDir}/config/routes.coffee"
          templateSource = '\n  server.route resource { name: \'{{item}}\', controller: ' +
            'require \'../app/controllers/{{itemPlural}}-controller\' }'
          template = Handlebars.compile templateSource
          result = template
            item: newItem
            itemPlural: newItemPlural
          fs.appendFileAsync routeFilePath, result
          .then ->
            console.log 'Added route to config/routes.coffee'
            resolve()
      .catch (err) ->
        reject err

  createControllerSpec = (newItem, newItemCapitalized, newItemPlural, newItemPluralCaptialized, projectDir) ->
    return new Promise (resolve, reject) ->
      controllersSpecsDir = path.normalize "#{projectDir}/test/server"
      controllerSpecPath = path.normalize "#{controllersSpecsDir}/#{newItemPlural}-controller-specs.coffee"
      mkdirp controllersSpecsDir
      .then -> fs.readFileAsync path.normalize("#{templateDir}/controller-specs.handlebars"), encoding: 'utf8'
      .then (data) ->
        template = Handlebars.compile data
        result = template
          item: newItem
          itemCapitalized: newItemCapitalized
          itemPlural: newItemPlural
          itemPluralCaptialized: newItemPluralCaptialized
        fs.writeFileAsync controllerSpecPath, result
        .then ->
          console.log "Created: #{controllerSpecPath}"
          resolve()
      .catch (err) ->
        reject err

  run: ->
    _self = @

    projectDir = _self.projectDir
    item = _self.newItem
    itemCapitalized = item.capitalize()
    itemPlural = pluralize.plural item
    itemPluralCaptialized = itemPlural.capitalize()

    return new Promise (resolve, reject) ->
      createController item, itemCapitalized, itemPlural, itemPluralCaptialized, projectDir
      .then -> createRoute item, itemPlural, projectDir
      .then -> createControllerSpec item, itemCapitalized, itemPlural, itemPluralCaptialized, projectDir
      .then ->
        resolve()
      .catch (err) ->
        reject err



module.exports = ControllerGenerator