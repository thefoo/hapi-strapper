#
# Created by Mike Linde <mlinde@lintechsol.com> on 23/05/15.
#
program = require 'commander'

ProjectGenerator = require './generators/project'
ModelGenerator = require './generators/model'
ScaffoldGenerator = require './generators/scaffold'

run = ->
  itemType = null
  program
  .version '1.0.0'
  .option '-n, --new <project_name>', 'Create a new Hapi.js project.'
  .option '-s, --scaffold <item_name>', 'Create a new model, controller (with route), and views'
  #.option '-c, --controller <item_name>', 'Create a new API controller (with route)'
  .option '-m, --model <item_name>', 'Create a new model'
  .option '-f, --force', 'Force overwriting (BEWARE!!!)'
  .action (newItem) ->
    itemType = newItem
  .parse process.argv

  # Output the help if no arguments are specified
  args = process.argv.slice(2)
  if not args.length
    program.outputHelp()
    process.exit(0)
  else if args.length is 1 and program.force?
    program.outputHelp()
    process.exit(0)

  # Make sure that only one of the operations is specified
  countOfOptions = 0
  countOfOptions++ if program['new']
  countOfOptions++ if program.scaffold
  countOfOptions++ if program.controller
  countOfOptions++ if program.model
  if countOfOptions > 1
    console.error 'You cannot run more than one operation at a time!'
    process.exit(1)

  if program['new']?
    generator = new ProjectGenerator program['new'], process.cwd()
    generator.run()
    .then -> console.log "\n#{program['new']} has been successfully generated"
    .catch (err) -> console.error "\n#{program['new']} could not be generated due to: #{err}"

  if program.model?
    generator = new ModelGenerator program.model, process.cwd()
    generator.run()
    .then -> console.log "\n#{program.model} has been successfully generated"
    .catch (err) -> console.error "\n#{program.model} could not be generated due to: #{err}"

  if program.scaffold?
    generator = new ScaffoldGenerator program.scaffold, process.cwd()
    generator.run()
    .then -> console.log "\nThe scaffolding for #{program.scaffold} has been successfully generated"
    .catch (err) -> "\nThe scaffolding for #{program.scaffold} could not be generated due to: #{err}"

exports.run = run