#! node_modules/.bin/coffee
#
# Created by Mike Linde <mlinde@lintechsol.com> on 22/05/15.
#
Promise = require 'bluebird'
_ = require 'underscore'

getRoutes = (server) ->
  return new Promise (resolve, reject) ->
    routeTable = []

    try
      hapiTable = server.table()[0].table
      for routeInTable in hapiTable
        do (routeInTable) ->
          route =
            method: routeInTable.method
            path: routeInTable.path
          if routeInTable.settings.tags?
            route.tags = routeInTable.settings.tags
          if routeInTable.settings.id?
            route.id = routeInTable.settings.id
          routeTable.push route

      routeTable = _.sortBy routeTable, (route) ->
        route.path

      resolve routeTable
    catch err
      reject err

module.exports = getRoutes